:: Copyright (C) 2022 Huawei Device Co., Ltd.
:: Licensed under the Apache License, Version 2.0 (the "License");
:: you may not use this file except in compliance with the License.
:: You may obtain a copy of the License at
::
::     http://www.apache.org/licenses/LICENSE-2.0
::
:: Unless required by applicable law or agreed to in writing, software
:: distributed under the License is distributed on an "AS IS" BASIS,
:: WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
:: See the License for the specific language governing permissions and
:: limitations under the License.
hdc_std file send %CD%\dummyframes.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/dummyframes.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\dummyframes.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/dummyframes.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\incompletempegframe.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/incompletempegframe.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\image.png /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/image.png /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\incompletempegframe.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/incompletempegframe.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std shell sh -c 'cp /data/local/tmp/image.png /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\notags.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/notags.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\notanmp3.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/notanmp3.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\obsolete.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/obsolete.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v1andv23andcustomtags.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v1andv23andcustomtags.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v1andv23tags.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v1andv23tags.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v1andv23tagswithalbumimage.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v1andv23tagswithalbumimage.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v1andv23tagswithalbumimage_utf16le.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v1andv23tagswithalbumimage_utf16le.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v1andv24tags.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v1andv24tags.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v1tag.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v1tag.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v1tagwithnotrack.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v1tagwithnotrack.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v23tag.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v23tag.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v23tagwithbpm.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v23tagwithbpm.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v23tagwithbpmfloat.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v23tagwithbpmfloat.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v23tagwithbpmfloatwithcomma.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v23tagwithbpmfloatwithcomma.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v23tagwithchapters.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v23tagwithchapters.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v23tagwithwmprating.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v23tagwithwmprating.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v23unicodetags.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v23unicodetags.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\v24tagswithalbumimage.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/v24tagswithalbumimage.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
hdc_std file send %CD%\withitunescomment.mp3 /data/local/tmp/
hdc_std shell sh -c 'cp /data/local/tmp/withitunescomment.mp3 /data/app/el2/100/base/cn.openharmony.mp3agic/haps/entry/files/'
echo %CD%
pause

